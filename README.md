# Programming
## Awesome
- [awesome](https://github.com/sindresorhus/awesome)
- [Awesome design patterns](https://github.com/DovAmir/awesome-design-patterns)

## Code quality
- [Завяжите шнурки и подтяните свои штаны!](https://habr.com/post/151110/)
- [Главный принцип хорошего кода](https://habr.com/post/150868/)
- [How to start writing high quality code at any point of your programming journey](https://medium.com/the-andela-way/how-to-start-writing-high-quality-code-at-any-point-of-your-programming-journey-d434cb0ba8ca)

## Programming principlec
- [Что такое SOLID, KISS, DRY и YAGNI?](https://www.youtube.com/watch?v=0qP6Vh8GNM0&t=0s&index=6&list=WL)

### Architecture
- [Pattern: Microservice Architecture](http://microservices.io/patterns/microservices.html)
- [Создание архитектуры программы или как проектировать табуретку](https://habr.com/post/276593/)
- [Масштабируемая веб-архитектура и распределенные системы](https://habr.com/post/185636/)
- [The System Design Primer](https://github.com/donnemartin/system-design-primer)